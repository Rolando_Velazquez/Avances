/// <reference path="jquery.d.ts">
var Persona = /** @class */ (function () {
    function Persona(n, a, e) {
        this.nombre = n;
        this.apellido = a;
        this.edad = e;
    }
    Persona.prototype.Suma = function (n1, n2) {
        return n1 + n2;
    };
    ;
    return Persona;
}());
window.onload = function () {
    var miPersona = new Persona("José", "Pérez", 35);
    var txtBox = document.createElement('input');
    txtBox.style.width = "250px";
    // txtBox.style.backgroundColor = "pink";
    txtBox.style.color = "black";
    txtBox.id = "text1";
    document.body.appendChild(txtBox);
    var salto = document.createElement("br");
    document.body.appendChild(salto);
    document.getElementById("text1").style.background = "blue";
    var resultado = document.createElement("text");
    resultado.innerHTML = "La suma de 23 y 6 es <b>" +
        miPersona.Suma(23, 6) + " </b>";
    document.body.appendChild(resultado);
};
